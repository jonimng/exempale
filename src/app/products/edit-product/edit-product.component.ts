import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  // Instance Variables
  id;
  product;
  model;
  manufacturer;
  service: ProductsService;
  // Form Builder
  productForm = new FormGroup({
      manufacturer: new FormControl(),
      model: new FormControl(),
      id: new FormControl()
  });

  sendData() {
    this.productForm.value.id = this.id;

    this.service.updateProduct(this.productForm.value).subscribe(
      response => {
        this.router.navigate(['/products']);
      }
    );
  }

  constructor(service: ProductsService, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.service = service;
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = + params.get('id'); // This line converts id from string into num
      this.service.getProduct(this.id).subscribe(response => {
        this.product = response.json();
        this.manufacturer = this.product.manufacturer,
        this.model = this.product.model;
      });
    });
  }

  ngOnInit() {
  }

}
