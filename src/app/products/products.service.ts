import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import { environment } from '../../environments/environment';



// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Injectable()
export class ProductsService {

  // Variables
  http: Http;

  getProducts() {

	   // tslint:disable-next-line:indent
	   return this.http.get(environment.url + '/cars?token=secretetoken');
  }

  searchProducts(name) {
    // tslint:disable-next-line:prefer-const
    let params = new HttpParams().append('name', name);
    // tslint:disable-next-line:prefer-const
    let options =  {
        headers: new Headers({
          'content-type': 'application/x-www-form-urlencoded'
        })
    };
    return this.http.post(environment.url + '/products/search', params.toString(), options);
  }

  getProduct(key) {
     return this.http.get(environment.url + '/cars/' + key);
  }
  deleteMessage(key) {

    return this.http.delete(environment.url + 'cars/' + key);
  }

  updateProduct(data) {
    // tslint:disable-next-line:prefer-const
    let options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded'
      })
    };

    // tslint:disable-next-line:prefer-const
    let params = new HttpParams().append('name', data.manufacturer).append('price', data.model);

    return this.http.put(environment.url + 'cars/' + data.id, params.toString(), options);
  }

  getProductsFireBase() {
    return this.db.list('/cars').valueChanges();
  }
    login(credentials) {
    const options = {
      headers: new Headers({
        'content-type': 'application/x-www-form-urlencoded'
      })
    };
     const params = new HttpParams().append('user', credentials.user).append('password', credentials.password);
         return this.http.post('environment.url' + '/auth',
         params.toString(),
         options).map(response => {
            const token = response.json().token;
            if (token) { localStorage.setItem('token', token); }

         });



  }

  constructor(http: Http, private db: AngularFireDatabase) {
    this.http = http;
  }

}
