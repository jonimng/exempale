import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ProductsService } from '../products.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

  // Variables	  
  productsKeys = [];

  constructor(private service:ProductsService, private router:Router) {  	
  	service.getProductsFireBase().subscribe(response=>{       		
    	this.productsKeys = Object.values(response);    	
 	  });
  }

  ngOnInit() {
  }

}
